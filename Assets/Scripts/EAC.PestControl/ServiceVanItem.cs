
namespace EAC.PestControl
{
    // unity namespaces
    using UnityEngine.EventSystems;
    using UnityEngine;

    // ======================================================================
    // ServiceVanItem

    public class ServiceVanItem
        : MonoBehaviour
        , IPointerClickHandler
    {
        public PestControlItem  itemType;
        public InventoryManager inventoryManager;

        private AudioSource m_audioSource;

        protected void Awake ()
        {
            m_audioSource = GetComponent<AudioSource>();
        }

        public void OnPointerClick (PointerEventData eventData)
        {
            inventoryManager.AddItem(itemType);
        }

        public void OnItemAdded ()
        {
            m_audioSource.Play();
        }

        public void OnItemRemoved ()
        {
        }
    };

} // namespace EAC.PestControl
