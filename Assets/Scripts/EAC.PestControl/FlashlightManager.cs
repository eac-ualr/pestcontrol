
namespace EAC.PestControl
{
    // unity namespaces
    using UnityEngine.Assertions;
    using UnityEngine;

    // ======================================================================
    // FlashlightManager

    public class FlashlightManager
        : MonoBehaviour
    {
        [Tooltip("GameObject for the flashlight")]
        public GameObject flashlight;

        public static FlashlightManager instance
        {
            get
            {
                if (m_singleton == null)
                {
                    m_singleton = FindSingleton();
                    m_singleton.InitializeSingleton();
                }

                return m_singleton;
            }
        }

        private Animator m_animator = null;
        private AudioSource m_audioSource = null;

        private static int m_paramIdFlashlightOn = Animator.StringToHash("FlashlightOn");
        private static FlashlightManager m_singleton = null;

        public void SetFlashlightEnabled (bool enabled)
        {
            m_animator.SetBool(m_paramIdFlashlightOn, enabled);
        }

        public void EnableFlashlight ()
        {
            SetFlashlightEnabled(true);
            m_audioSource.Play();
        }

        public void DisableFlashlight ()
        {
            SetFlashlightEnabled(false);
            m_audioSource.Play();
        }

        // ------------------------------------------------------------------

        private static FlashlightManager FindSingleton ()
        {
            FlashlightManager manager
                = (FlashlightManager) FindObjectOfType(typeof(FlashlightManager));
            Assert.IsNotNull(manager);

            return manager;
        }

        private void InitializeSingleton ()
        {
            Assert.IsNotNull(flashlight);

            m_animator = flashlight.GetComponent<Animator>();
            Assert.IsNotNull(m_animator);
            m_audioSource = flashlight.GetComponent<AudioSource>();
            Assert.IsNotNull(m_audioSource);
        }
    };

} // namespace EAC.PestControl
