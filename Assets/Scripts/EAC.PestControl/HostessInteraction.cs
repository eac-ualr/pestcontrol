
namespace EAC.PestControl
{
    // unity namespaces
    using UnityEngine.Assertions;
    using UnityEngine.UI;
    using UnityEngine;
    // local namespaces
    using EAC.Navigation;
    using EAC.Utility;

    // ======================================================================
    // HostessInteraction

    public class HostessInteraction
        : MonoBehaviour
    {
        [Tooltip("GameObject for the hostess.")]
        public GameObject hostess;

        private enum InteractionState
        {
            Introduction,

            ReplyTable,
            DirectToKitchen_1,
            DirectToKitchen_2,
        };

        private InteractionState m_state = InteractionState.Introduction;
        private GameObject m_panelHostessGO;
        private Text m_textHostess;
        private GameObject m_panelTechnicianGO;

        private Animator m_animator = null;
        private AudioSource m_audioSource = null;
        private static int m_paramIdTalking = Animator.StringToHash("Talking");

        private static readonly string kTextReplyTable
            = "Oh, so sorry, but we will not open for another hour.\n";
        private static readonly string kTextDirectToKitchen1
            = "Hello and thank you for stopping by so quickly!\n"
            + "\n"
            + "Please check in with our kitchen staff, they can give you more details about the "
            + "pests they have spotted. You can go right through our dining area to the kitchen.";
        private static readonly string kTextDirectToKitchen2
            = "Hello, you must be with the pest control company then?\n"
            + "\n"
            + "Please check in with our kitchen staff, they can give you more details about the "
            + "pests they have spotted. You can go right through our dining area to the kitchen.";

        public void Awake ()
        {
            m_animator = hostess.GetComponent<Animator>();
            m_animator.SetBool(m_paramIdTalking, false);

            m_audioSource = hostess.GetComponentInChildren<AudioSource>();

            m_panelHostessGO = ObjectFinder.FindChild(hostess, "Panel_hostess_worker");
            m_textHostess = ObjectFinder.GetChildComponent<Text>(m_panelHostessGO, "Text");
            m_panelTechnicianGO = ObjectFinder.FindChild(hostess, "Panel_pest_control_technician");
        }

        public void OnEnable ()
        {
            UpdateState();
        }

        public void OnPostArrive (LocationManager locationManager, int prevLocationIndex)
        {
            m_animator.SetBool(m_paramIdTalking, true);
        }

        public void OnPreLeave (LocationManager locationManager, int newLocationIndex)
        {
            m_animator.SetBool(m_paramIdTalking, false);
        }

        public void OnStateEnter_Idle ()
        {
            m_audioSource.Play();
        }

        public void OnStateExit_Idle ()
        {
            m_audioSource.Stop();
        }

        public void OnButtonIntro (int introChoiceIdx)
        {
            Assert.IsTrue(m_state == InteractionState.Introduction);

            if (introChoiceIdx == 1)
            {
                m_state = InteractionState.ReplyTable;
                UpdateState();
            }
            else if (introChoiceIdx == 2)
            {
                m_state = InteractionState.DirectToKitchen_1;
                UpdateState();
            }
            else if (introChoiceIdx == 3)
            {
                m_state = InteractionState.DirectToKitchen_2;
                UpdateState();
            }
            else
            {
                Assert.IsTrue(false, "Unexpected introChoiceIdx!");
            }
        }

        public void ResetState ()
        {
            m_state = InteractionState.Introduction;
            UpdateState();
        }

        // -------------------------------------------------------------------

        private void UpdateState ()
        {
            switch (m_state)
            {
            case InteractionState.Introduction:
                m_panelHostessGO.SetActive(false);
                m_panelTechnicianGO.SetActive(true);
                break;

            case InteractionState.ReplyTable:
                m_panelHostessGO.SetActive(true);
                m_textHostess.text = kTextReplyTable;
                m_panelTechnicianGO.SetActive(false);
                m_state = InteractionState.Introduction;
                break;

            case InteractionState.DirectToKitchen_1:
                m_panelHostessGO.SetActive(true);
                m_textHostess.text = kTextDirectToKitchen1;
                m_panelTechnicianGO.SetActive(false);
                break;

            case InteractionState.DirectToKitchen_2:
                m_panelHostessGO.SetActive(true);
                m_textHostess.text = kTextDirectToKitchen2;
                m_panelTechnicianGO.SetActive(false);
                break;
            }
        }
    };

} // namespace EAC.PestControl
