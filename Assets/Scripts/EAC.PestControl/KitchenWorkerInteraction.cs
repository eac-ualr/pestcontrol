
namespace EAC.PestControl
{
    // system namespaces
    using System;
    // unity namespaces
    using UnityEngine;
    // local namespaces
    using EAC.Navigation;

    // ======================================================================
    // KitchenWorkerInteraction

    public class KitchenWorkerInteraction
        : MonoBehaviour
    {
        private Animator m_animator = null;
        private AudioSource m_audioSource = null;
        private static int m_paramIdTalking = Animator.StringToHash("Talking");

        public void Start ()
        {
            m_animator = GetComponent<Animator>();
            m_animator.SetBool(m_paramIdTalking, false);

            m_audioSource = GetComponentInChildren<AudioSource>();
        }

        public void OnPostArrive (LocationManager locationManager, int prevLocationIndex)
        {
            m_animator.SetBool(m_paramIdTalking, true);
        }

        public void OnPreLeave (LocationManager locationManager, int newLcationIndex)
        {
            m_animator.SetBool(m_paramIdTalking, false);
        }

        public void OnStateEnter_Stand ()
        {
            m_audioSource.Play();
        }

        public void OnStateExit_Stand ()
        {
            m_audioSource.Stop();
        }
    };

} // namespace EAC.PestControl
