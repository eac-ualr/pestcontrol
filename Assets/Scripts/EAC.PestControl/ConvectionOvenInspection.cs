
namespace EAC.PestControl
{
    // unity namespaces
    using UnityEngine.Assertions;
    using UnityEngine;
    // local namespaces
    using EAC.Navigation;

    // ======================================================================
    // ConvectionOvenInspection

    public class ConvectionOvenInspection
        : PestInspection
    {
        [Tooltip("GameObject for the convection oven.")]
        public GameObject convectionOven;

        private Animator m_animatorOven;
        private static int m_paramIdInspecting = Animator.StringToHash("Inspecting");

        public void Start ()
        {
            Assert.IsNotNull(convectionOven);

            m_animatorOven = convectionOven.GetComponent<Animator>();
            m_animatorOven.SetBool(m_paramIdInspecting, false);
        }

        public override void OnPostArrive (LocationManager locationManager, int prevLocationIndex)
        {
            base.OnPostArrive(locationManager, prevLocationIndex);
            m_animatorOven.SetBool(m_paramIdInspecting, true);
        }

        public override void OnPreLeave (LocationManager locationManager, int newLocationIndex)
        {
            base.OnPreLeave(locationManager, newLocationIndex);
            m_animatorOven.SetBool(m_paramIdInspecting, false);
        }
    };

} // namespace EAC.PestControl
