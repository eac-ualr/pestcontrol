
namespace EAC.PestControl
{
    // system namespaces
    using System.Collections.Generic;
    // unity namespaces
    using UnityEngine.Assertions;
    using UnityEngine;

    // ======================================================================
    // PestsFlashlightInteraction

    public class PestsFlashlightInteraction
        : MonoBehaviour
    {
        [Tooltip("Canvas for displaying extra info")]
        public GameObject infoCanvas;

        private Animator            m_canvasAnimator = null;
        private HashSet<GameObject> m_contacts = new HashSet<GameObject>();
        private static readonly int m_paramIdVisible = Animator.StringToHash("Visible");

        #region Unity Messages
        protected void Awake ()
        {
            Assert.IsNotNull(infoCanvas);

            m_canvasAnimator = infoCanvas.GetComponent<Animator>();
            Assert.IsNotNull(m_canvasAnimator);
            m_canvasAnimator.SetBool(m_paramIdVisible, false);
        }

        protected void OnTriggerEnter (Collider collider)
        {
            if (collider.gameObject.tag == "Flashlight")
            {
                m_contacts.Add(collider.gameObject);
            }

            if (m_contacts.Count == 1)
            {
                m_canvasAnimator.SetBool(m_paramIdVisible, true);
            }
        }

        protected void OnTriggerExit (Collider collider)
        {
            if (collider.gameObject.tag == "Flashlight")
            {
                m_contacts.Remove(collider.gameObject);
            }

            if (m_contacts.Count == 0)
            {
                m_canvasAnimator.SetBool(m_paramIdVisible, false);
            }
        }
        #endregion
    };

} // namespace EAC.PestControl
