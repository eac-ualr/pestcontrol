
namespace EAC.PestControl
{
    // unity namespaces
    using UnityEngine.Assertions;
    using UnityEngine;
    // local namespaces
    using EAC.Navigation;
    using EAC.Utility;

    // ======================================================================
    //

    public class ApplicationStateManager
        : MonoBehaviour
    {
        public static ApplicationStateManager instance
        {
            get
            {
                FindSingleton();
                return m_singleton;
            }
        }

        public HostessInteraction hostessInteraction;

        private static ApplicationStateManager m_singleton = null;

        public void ResetState ()
        {
            PlayerStateManager.instance.ResetState();
            LocationManager.instance.ResetState();
            hostessInteraction.ResetState();
        }

        // ------------------------------------------------------------------

        private static void FindSingleton ()
        {
            if (m_singleton != null)
                return;

            ApplicationStateManager[] managers
                = (ApplicationStateManager[]) FindObjectsOfType(typeof(ApplicationStateManager));

            if (managers.Length == 1)
            {
                m_singleton = managers[0];
                m_singleton.InitSingleton();
            }
            else if (managers.Length > 1)
            {
                EACDebug.LogError(
                    "Multiple instances of ApplicationStateManager singleton in scene!");
            }
            else
            {
                EACDebug.LogError("No instance of ApplicationStateManager singleton in scene!");
            }
        }

        private void InitSingleton ()
        {
            Assert.IsNotNull(hostessInteraction);
        }
    };

} // namespace EAC.PestControl
