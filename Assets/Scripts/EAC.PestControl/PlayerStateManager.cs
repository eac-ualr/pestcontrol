
namespace EAC.PestControl
{
    // unity namespaces
    using UnityEngine;
    // local namespaces
    using EAC.Utility;

    // ======================================================================
    //

    public class PlayerStateManager
        : MonoBehaviour
    {
        public static PlayerStateManager instance
        {
            get
            {
                FindSingleton();
                return m_singleton;
            }
        }

        private Inventory m_inventory;

        private static PlayerStateManager m_singleton = null;

        #region Unity Messages
        protected void Awake ()
        {
            FindSingleton();
        }
        #endregion

        public Inventory GetInventory ()
        {
            return m_inventory;
        }

        public void ResetState ()
        {
            m_inventory.ClearInventory();
        }

        // ------------------------------------------------------------------

        private static void FindSingleton ()
        {
            if (m_singleton != null)
                return;

            PlayerStateManager[] managers
                = (PlayerStateManager[]) FindObjectsOfType(typeof(PlayerStateManager));

            if (managers.Length == 1)
            {
                m_singleton = managers[0];
                m_singleton.InitSingleton();
            }
            else if (managers.Length > 1)
            {
                EACDebug.LogError("Multiple instances of PlayerStateManager singleton in scene!");
            }
            else
            {
                EACDebug.LogError("No instance of PlayerStateManager singleton in scene!");
            }
        }

        private void InitSingleton ()
        {
            DontDestroyOnLoad(this);
            m_inventory = ObjectFinder.GetOrCreateComponent<Inventory>(gameObject);
        }
    };

} // namespace EAC.PestControl
