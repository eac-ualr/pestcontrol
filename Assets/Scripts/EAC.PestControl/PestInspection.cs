
namespace EAC.PestControl
{
    // unity namespaces
    using UnityEngine;
    // local namespaces
    using EAC.Navigation;

    // ======================================================================
    // PestInspection
    //
    // Base class for interaction scripts for "pest locations".

    public class PestInspection
        : MonoBehaviour
    {
        public virtual void OnPostArrive (LocationManager locationManager, int prevLocationIndex)
        {
            FlashlightManager.instance.EnableFlashlight();
        }

        public virtual void OnPreLeave (LocationManager locationManager, int newLocationManager)
        {
            FlashlightManager.instance.DisableFlashlight();
        }
    };

} // namespace EAC.PestControl
