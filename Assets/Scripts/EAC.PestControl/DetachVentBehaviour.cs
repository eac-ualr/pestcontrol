
namespace EAC.PestControl
{
    // unity namespaces
    using UnityEngine.Assertions;
    using UnityEngine;

    // ======================================================================
    // DetachVentBehaviour
    //
    // Behaviour for the convection_oven vent, triggering sound effect when
    // detaching.

    public class DetachVentBehaviour
        : StateMachineBehaviour
    {
        private AudioSource m_audioSource = null;

        public override void OnStateEnter (
            Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            InitAudioSource(animator);
            m_audioSource.Play();
        }

        public override void OnStateExit (Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            InitAudioSource(animator);
            m_audioSource.Stop();
        }

        // ------------------------------------------------------------------

        private void InitAudioSource (Animator animator)
        {
            if (m_audioSource == null)
                m_audioSource = animator.GetComponentInChildren<AudioSource>();
        }
    };

} // namespace EAC.PestControl
