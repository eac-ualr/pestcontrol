
namespace EAC.PestControl
{
    // system namespaces
    using System.Collections.Generic;
    // unity namespaces
    using UnityEngine.Assertions;
    using UnityEngine;
    // local namespaces
    using EAC.Utility;

    // ======================================================================
    // Inventory

    public class Inventory
        : MonoBehaviour
    {
        private Dictionary<PestControlItem, int> m_inventory;

        protected void Awake ()
        {
            InitInventory();
        }

        public int AddItem (PestControlItem item, int count = 1)
        {
            Assert.IsTrue(count >= 1);
            Assert.IsTrue(m_inventory.ContainsKey(item));

            int itemCount = m_inventory[item] + count;
            m_inventory[item] = itemCount;

            return itemCount;
        }

        public int RemoveItem (PestControlItem item, int count = 1)
        {
            Assert.IsTrue(count >= 1);
            Assert.IsTrue(m_inventory.ContainsKey(item));

            int itemCount = m_inventory[item] - count;
            Assert.IsTrue(itemCount >= 0);
            m_inventory[item] = itemCount;

            return itemCount;
        }

        public int GetItemCount (PestControlItem item)
        {
            Assert.IsTrue(m_inventory.ContainsKey(item));

            return m_inventory[item];
        }

        public bool ContainsItem (PestControlItem item)
        {
            return GetItemCount(item) > 0;
        }

        public void ClearInventory ()
        {
            foreach (PestControlItem item in System.Enum.GetValues(typeof(PestControlItem)))
            {
                m_inventory[item] = 0;
            }
        }

        public void DumpInventory ()
        {
            foreach (PestControlItem item in System.Enum.GetValues(typeof(PestControlItem)))
            {
                if (ContainsItem(item))
                {
                    EACDebug.LogFormat("Inventory has {0}x {1}", GetItemCount(item), item);
                }
            }
        }

        // ------------------------------------------------------------------

        private void InitInventory ()
        {
            m_inventory = new Dictionary<PestControlItem, int>();
            ClearInventory();
        }
    };

} // namespace EAC.PestControl