
namespace EAC.PestControl
{
    // ======================================================================
    // PestControlItem

    public enum PestControlItem
    {
        BaitTrap,
        BugSprayCan,
        ElectromagneticTrap,
        PestControlGel,
        ScentTrap,
        StickyTrap,
    };

} // namespace EAC.PestControl
