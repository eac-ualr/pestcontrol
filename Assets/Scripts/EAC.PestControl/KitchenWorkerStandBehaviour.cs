
namespace EAC.PestControl
{
    // unity namespaces
    using UnityEngine.Assertions;
    using UnityEngine;
    // local namespaces
    using EAC.Utility;

    // ======================================================================
    // KitchenWorkerStandBehaviour
    //
    // Behaviour for the kitchen_worker, triggering sound effect when
    // interacting.

    public class KitchenWorkerStandBehaviour
        : StateMachineBehaviour
    {
        public override void OnStateEnter (
            Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            KitchenWorkerInteraction kwi = animator.GetComponent<KitchenWorkerInteraction>();
            kwi.OnStateEnter_Stand();
        }

        public override void OnStateExit (
            Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            KitchenWorkerInteraction kwi = animator.GetComponent<KitchenWorkerInteraction>();
            kwi.OnStateExit_Stand();
        }
    };

} // namespace EAC.PestControl
