
namespace EAC.PestControl
{
    // unity namespaces
    using UnityEngine.Assertions;
    using UnityEngine;
    // local namespaces
    using EAC.Utility;

    // ======================================================================
    // HostessIdleBehaviour

    public class HostessIdleBehaviour
        : StateMachineBehaviour
    {
        public override void OnStateEnter (
            Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            HostessInteraction hi = animator.GetComponentInChildren<HostessInteraction>();
            hi.OnStateEnter_Idle();
        }

        public override void OnStateExit (
            Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            HostessInteraction hi = animator.GetComponentInChildren<HostessInteraction>();
            hi.OnStateExit_Idle();
        }
    };

} // namespace EAC.PestControl
