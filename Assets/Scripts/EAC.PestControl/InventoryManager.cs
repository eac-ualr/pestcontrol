
namespace EAC.PestControl
{
    // system namespaces
    using System.Collections.Generic;
    // unity namespaces
    using UnityEngine.Assertions;
    using UnityEngine;

    // ======================================================================
    // InventoryManager

    public class InventoryManager
        : MonoBehaviour
    {
        public ServiceVanItem[] serviceVanItems;
        public SupplyBagItem[] supplyBagItems;

        private Inventory m_inventory;
        private Dictionary<PestControlItem, ServiceVanItem> m_serviceVanItemMap;
        private Dictionary<PestControlItem, SupplyBagItem> m_supplyBagItemMap;

        protected void Start ()
        {
            m_inventory = PlayerStateManager.instance.GetInventory();

            InitItemMaps();
            InitItemVisibility();
        }

        public void AddItem (PestControlItem item, int count = 1)
        {
            int itemCount = m_inventory.AddItem(item, count);
            UpdateItemVisibility(item, itemCount);
        }

        public void RemoveItem (PestControlItem item, int count = 1)
        {
            int itemCount = m_inventory.RemoveItem(item, count);
            UpdateItemVisibility(item, itemCount);
        }

        public bool ContainsItem (PestControlItem item)
        {
            return m_inventory.ContainsItem(item);
        }

        // ------------------------------------------------------------------

        private void UpdateItemVisibility (PestControlItem item, int count)
        {
            Assert.IsTrue(m_supplyBagItemMap.ContainsKey(item));
            Assert.IsTrue(m_serviceVanItemMap.ContainsKey(item));

            if (count > 0)
            {
                m_supplyBagItemMap[item].gameObject.SetActive(true);
                m_supplyBagItemMap[item].OnItemAdded();
                m_serviceVanItemMap[item].gameObject.SetActive(false);
                m_serviceVanItemMap[item].OnItemRemoved();
            }
            else
            {
                m_supplyBagItemMap[item].gameObject.SetActive(false);
                m_supplyBagItemMap[item].OnItemRemoved();
                m_serviceVanItemMap[item].gameObject.SetActive(true);
                m_serviceVanItemMap[item].OnItemAdded();
            }
        }

        private void InitItemMaps ()
        {
            m_serviceVanItemMap
                = new Dictionary<PestControlItem, ServiceVanItem>(serviceVanItems.Length);
            foreach (ServiceVanItem item in serviceVanItems)
            {
                m_serviceVanItemMap.Add(item.itemType, item);
            }

            m_supplyBagItemMap
                = new Dictionary<PestControlItem, SupplyBagItem>(supplyBagItems.Length);
            foreach (SupplyBagItem item in supplyBagItems)
            {
                m_supplyBagItemMap.Add(item.itemType, item);
            }
        }

        private void InitItemVisibility ()
        {
            foreach (PestControlItem item in System.Enum.GetValues(typeof(PestControlItem)))
            {
                if (m_inventory.ContainsItem(item))
                {
                    m_supplyBagItemMap[item].gameObject.SetActive(true);
                    m_serviceVanItemMap[item].gameObject.SetActive(false);
                }
                else
                {
                    m_supplyBagItemMap[item].gameObject.SetActive(false);
                    m_serviceVanItemMap[item].gameObject.SetActive(true);
                }
            }
        }
    };

} // namespace EAC.PestControl
