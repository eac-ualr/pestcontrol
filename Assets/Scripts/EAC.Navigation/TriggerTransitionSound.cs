
namespace EAC.Navigation
{
    // unity namespaces
    using UnityEngine;

    public class TriggerTransitionSound
        : MonoBehaviour
    {
        public AudioClip    audioClip;
        public AudioSource  audioSource;

        public void OnPreLeave (LocationManager locationManager, int newLocationIndex)
        {
            audioSource.PlayOneShot(audioClip);
        }
    };

} // namespace EAC.Navigation
