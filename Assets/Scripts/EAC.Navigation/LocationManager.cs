
namespace EAC.Navigation
{
    // system namespaces
    using System.Collections.Generic;
    // unity namespaces
    using UnityEngine.Assertions;
    using UnityEngine.Events;
    using UnityEngine.VR;
    using UnityEngine;
    // local namespaces
    using EAC.Rendering;

    // ======================================================================
    // Event Types

    [System.Serializable]
    public class LocationLeaveEvent
        : UnityEvent<LocationManager, int>
    {
    }

    [System.Serializable]
    public class LocationArriveEvent
        : UnityEvent<LocationManager, int>
    {
    }

    // ======================================================================
    // LocationManager

    public class LocationManager
        : MonoBehaviour
    {
        [Tooltip("Transform that is moved to locations.")]
        public Transform            targetTransform;
        [Tooltip("Index of the startup location.")]
        public int                  initialLocationIndex;
        public LocationMarkerBase[] locations;

        public LocationLeaveEvent onPreLeave;
        public LocationLeaveEvent onPostLeave;
        public LocationArriveEvent onPreArrive;
        public LocationArriveEvent onPostArrive;

        public static LocationManager instance
        {
            get
            {
                if (m_singleton == null)
                    m_singleton = FindSingleton();

                return m_singleton;
            }
        }

        private int                 m_locationIndex = -1;
        private int                 m_prevLocationIndex = -1;
        private FadeScreenEffect    m_fadeEffect = null;

        private const float kFadeDuration = 0.25f;
        private static LocationManager m_singleton = null;

        public void Start ()
        {
            Assert.IsTrue(0 <= initialLocationIndex && initialLocationIndex < locations.Length);

            if (onPreLeave == null)
                onPreLeave = new LocationLeaveEvent();

            if (onPostLeave == null)
                onPostLeave = new LocationLeaveEvent();

            if (onPreArrive == null)
                onPreArrive = new LocationArriveEvent();

            if (onPostArrive == null)
                onPostArrive = new LocationArriveEvent();

            m_fadeEffect = Camera.main.GetComponent<FadeScreenEffect>();
            Assert.IsNotNull(m_fadeEffect);

            // default to transform of the gameObject
            if (targetTransform == null)
                targetTransform = GetComponent<Transform>();

            InitializeLocationData();
            TransitionLocation(initialLocationIndex);
        }

        public void SetLocation (int locationIndex)
        {
            TransitionLocation(locationIndex);
        }

        public void SetLocation (LocationMarkerBase marker)
        {
            TransitionLocation(marker.locationIndex);
        }

        public void ResetState ()
        {
            SetLocation(initialLocationIndex);
        }

        // ------------------------------------------------------------------

        private void TransitionLocation (int locationIndex)
        {
            if (m_locationIndex == locationIndex)
                return;

            // update location indices
            m_prevLocationIndex = m_locationIndex;
            m_locationIndex     = locationIndex;

            // notify old position we are about to leave
            if (0 <= m_prevLocationIndex && m_prevLocationIndex < locations.Length)
            {
                locations[m_prevLocationIndex].OnPreLeave(this, m_locationIndex);

                if (onPreLeave != null)
                    onPreLeave.Invoke(this, m_locationIndex);
            }

            m_fadeEffect.FadeOut(kFadeDuration, OnFadeOutComplete, null);
        }

        private void OnFadeOutComplete ()
        {
            // notify old position we have left
            if (0 <= m_prevLocationIndex && m_prevLocationIndex < locations.Length)
            {
                locations[m_prevLocationIndex].OnPostLeave(this, m_locationIndex);

                if (onPostLeave != null)
                    onPostLeave.Invoke(this, m_locationIndex);
            }

            // notify new position we are about to arrive
            if (0 <= m_locationIndex && m_locationIndex < locations.Length)
            {
                locations[m_locationIndex].OnPreArrive(this, m_prevLocationIndex);

                if (onPreArrive != null)
                    onPreArrive.Invoke(this, m_prevLocationIndex);
            }

            m_fadeEffect.FadeIn(kFadeDuration, OnFadeInComplete, null);
        }

        private void OnFadeInComplete ()
        {
            // notify new position we have arrived
            if (0 <= m_locationIndex && m_locationIndex < locations.Length)
            {
                locations[m_locationIndex].OnPostArrive(this, m_prevLocationIndex);

                if (onPostArrive != null)
                    onPostArrive.Invoke(this, m_prevLocationIndex);
            }
        }

        private void InitializeLocationData ()
        {
            for (int i = 0; i < locations.Length; ++i)
                locations[i].locationIndex = i;
        }

        private static LocationManager FindSingleton ()
        {
            LocationManager manager
                = (LocationManager) FindObjectOfType(typeof(LocationManager));
            Assert.IsNotNull(manager);

            return manager;
        }
    }

} // namespace EAC.Navigation
