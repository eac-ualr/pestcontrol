
namespace EAC.Navigation
{
    // unity namespaces
    using UnityEngine;

    // ======================================================================
    // ToggleLocationBehaviours
    //
    // Enable/Disable a list of behaviours that are related to a location.
    // Intended for use with the LocationMarker Events (onPreLeave,
    // onPostLeave, onPreArrive, onPostArrive).

    public class ToggleLocationBehaviours
        : ToggleLocationBase
    {
        public Behaviour[] behaviours;

        // ------------------------------------------------------------------

        protected override void OnEnableLocation ()
        {
            foreach (Behaviour b in behaviours)
            {
                b.enabled = true;
            }
        }

        protected override void OnDisableLocation ()
        {
            foreach (Behaviour b in behaviours)
            {
                b.enabled = false;
            }
        }
    };

} // namespace EAC.Navigation
