
namespace EAC.Navigation
{
    // unity namespaces
    using UnityEngine;

    // ======================================================================
    // ToggleLocationBase
    //
    // Calls OnEnableLocation/OnDisableLocation when a location is becoming
    // active/inactive.

    public abstract class ToggleLocationBase
        : MonoBehaviour
    {
        #region Unity Events
        protected void Start ()
        {
            // initially disable
            OnDisableLocation();
        }
        #endregion

        public void OnPostArrive (LocationManager locationManager, int prevLocationIndex)
        {
            OnEnableLocation();
        }

        public void OnPreLeave (LocationManager locationManager, int newLocationIndex)
        {
            OnDisableLocation();
        }

        // ------------------------------------------------------------------

        protected abstract void OnEnableLocation ();

        protected abstract void OnDisableLocation ();
    };

} // namespace EAC.Navigation
