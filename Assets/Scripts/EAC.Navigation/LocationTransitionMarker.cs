
namespace EAC.Navigation
{
    // unity namespaces
    using UnityEngine.EventSystems;
    using UnityEngine;
    // local namespaces
    using EAC.Utility;

    // ======================================================================
    // LocationTransitionMarker

    public class LocationTransitionMarker
        : MonoBehaviour
        , IPointerClickHandler
    {
        [Tooltip("Location to travel to when triggering the transition")]
        public LocationMarkerBase destination;

        public void OnPointerClick (PointerEventData eventData)
        {
            LocationManager.instance.SetLocation(destination.locationIndex);
        }
    }

} // namespace EAC.Navigation
