
namespace EAC.Navigation
{
    // unity namespaces
    using UnityEngine;
    // local namespaces
    using EAC.Utility;

    // ======================================================================
    // ToggleLocationSounds
    //
    // Enables/Disables a list of AudioSources. Intended for use with the
    // LocationMarker events (onPreLeave, onPostLeave, onPreArrive,
    // onPostArrive).

    public class ToggleLocationSounds
        : MonoBehaviour
    {
        public AudioSource[] audioSources;

        #region Unity Events
        protected void Awake ()
        {
            // initially all sounds are disabled
            DisableSounds();
        }
        #endregion Unity Events

        public void OnPreArrive (LocationManager locationManager, int prevLocationIndex)
        {
            EnableSounds();
        }

        public void OnPostLeave (LocationManager locationManager, int newLocationIndex)
        {
            DisableSounds();
        }

        // ------------------------------------------------------------------

        private void EnableSounds ()
        {
            foreach(AudioSource source in audioSources)
            {
                source.Play();
            }
        }

        private void DisableSounds ()
        {
            foreach(AudioSource source in audioSources)
            {
                source.Pause();
            }
        }
    };

} // namespace EAC.Navigation
