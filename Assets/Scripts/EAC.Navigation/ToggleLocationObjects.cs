
namespace EAC.Navigation
{
    // unity namespaces
    using UnityEngine;

    // ======================================================================
    // ToggleLocationObjects
    //
    // Enable/Disable a list of GameObjects that are related to a location.
    // Intended for use with the LocationMarker Events (onPreLeave,
    // onPostLeave, onPreArrive, onPostArrive).

    public class ToggleLocationObjects
        : ToggleLocationBase
    {
        public GameObject[] gameObjects;

        // ------------------------------------------------------------------

        protected override void OnEnableLocation ()
        {
            foreach (GameObject go in gameObjects)
            {
                go.SetActive(true);
            }
        }

        protected override void OnDisableLocation ()
        {
            foreach (GameObject go in gameObjects)
            {
                go.SetActive(false);
            }
        }
    };

} // namespace EAC.Navigation
