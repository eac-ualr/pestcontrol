
namespace EAC.Navigation
{
    // system namespaces
    using System.Collections.Generic;
    // unity namespaces
    using UnityEngine.VR;
    using UnityEngine;

    // ======================================================================
    // LocationMarker

    public class LocationMarker
        : LocationMarkerBase
    {
        [Tooltip("Locations that make up a 'sub-area'. When transitioning from these locations"
            + "the head position will not be reset.")]
        public LocationMarkerBase[] subAreaLocations;

        private HashSet<int>  m_subAreaIndexLUT = null;

        public override void OnPreArrive (LocationManager locationManager, int prevLocationIndex)
        {
            base.OnPreArrive(locationManager, prevLocationIndex);

            // change position
            Transform targetTransform = locationManager.targetTransform;
            targetTransform.position = transform.position;
            targetTransform.rotation = transform.rotation;

            RecenterHead(targetTransform, prevLocationIndex);
        }

        // ------------------------------------------------------------------

        private void RecenterHead (Transform targetTransform, int prevLocationIndex)
        {
            InitSubAreaIndexLUT();

            if (m_subAreaIndexLUT == null || !m_subAreaIndexLUT.Contains(prevLocationIndex))
            {
                // recenter head tracking
                InputTracking.Recenter();

#if UNITY_EDITOR
                GvrEditorEmulator editorEmulator = targetTransform.GetComponent<GvrEditorEmulator>();
                if (editorEmulator != null)
                    editorEmulator.Recenter();
#endif
            }
        }

        private void InitSubAreaIndexLUT ()
        {
            if (subAreaLocations.Length == 0 || m_subAreaIndexLUT != null)
                return;

            m_subAreaIndexLUT = new HashSet<int>();

            for (int i = 0; i < subAreaLocations.Length; ++i)
            {
                m_subAreaIndexLUT.Add(subAreaLocations[i].locationIndex);
            }
        }
    }

} // namespace EAC.Navigation
