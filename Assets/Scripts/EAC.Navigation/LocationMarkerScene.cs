
namespace EAC.Navigation
{
    // unity namespaces
    using UnityEngine.SceneManagement;
    using UnityEngine;

    // ======================================================================
    // LocationMarkerScene

    public class LocationMarkerScene
        : LocationMarkerBase
    {
        public string targetSceneName;

        public override void OnPreArrive (LocationManager locationManager, int prevLocationIndex)
        {
            base.OnPreArrive(locationManager, prevLocationIndex);

            SceneManager.LoadScene(targetSceneName);
        }
    };


} // namespace EAC.Navigation
