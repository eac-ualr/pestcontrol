
namespace EAC.Navigation
{
    // unity namespaces
    using UnityEngine;
    // local namespaces
    using EAC.Utility;

    // ======================================================================
    // LocationMarkerBase

    public class LocationMarkerBase
        : MonoBehaviour
    {
        public int locationIndex;

        public LocationLeaveEvent onPreLeave;
        public LocationLeaveEvent onPostLeave;
        public LocationArriveEvent onPreArrive;
        public LocationArriveEvent onPostArrive;

        #region Unity Messages
        protected void Awake ()
        {
            if (onPreLeave == null)
                onPreLeave = new LocationLeaveEvent();

            if (onPostLeave == null)
                onPostLeave = new LocationLeaveEvent();

            if (onPreArrive == null)
                onPreArrive = new LocationArriveEvent();

            if (onPostArrive == null)
                onPostArrive = new LocationArriveEvent();
        }
        #endregion

        public virtual void OnPreLeave (LocationManager locationManager, int newLocationIndex)
        {
            if (onPreLeave != null)
                onPreLeave.Invoke(locationManager, newLocationIndex);
        }

        public virtual void OnPostLeave (LocationManager locationManager, int newLocationIndex)
        {
            if (onPostLeave != null)
                onPostLeave.Invoke(locationManager, newLocationIndex);
        }

        public virtual void OnPreArrive (LocationManager locationManager, int prevLocationIndex)
        {
            if (onPreArrive != null)
                onPreArrive.Invoke(locationManager, prevLocationIndex);
        }

        public virtual void OnPostArrive (LocationManager locationManager, int prevLocationIndex)
        {
            if (onPostArrive != null)
                onPostArrive.Invoke(locationManager, prevLocationIndex);
        }
    }

} // namespace EAC.Navigation
