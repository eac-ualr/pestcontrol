
namespace EAC.Rendering
{
    // system namespaces
    using System;
    using System.Collections;
    // unity namespaces
    using UnityEngine;

    // ======================================================================
    // FadeScreenEffect
    //
    // Fades the screen to/from a given color over time.

    public class FadeScreenEffect
        : MonoBehaviour
    {
        [Tooltip("Color to fade out to.")]
        public Color fadeColor;

        private Coroutine m_activeFade = null;
        private Action    m_cancelCallback = null;
        private Material  m_blendMaterial;
        private float     m_blendFactor = 0.0f;

        public void Awake ()
        {
            Shader blendShader = Shader.Find("Hidden/EACFadeScreenEffect");
            m_blendMaterial = new Material(blendShader);

            enabled = false;
        }

        // Fade the screen out towards fadeColor.
        // The completeCallback (if not null) is called when the fade is complete, while
        // cancelCallback (if not null) is called when the current fade is canceled (e.g. by
        // starting a new fade).
        public void FadeOut (float fadeDuration, Action completeCallback, Action cancelCallback)
        {
            if (m_activeFade != null)
            {
                CancelActiveFade();
            }

            m_blendMaterial.SetColor("_FadeColor", fadeColor);
            enabled = true;
            m_cancelCallback = cancelCallback;
            m_activeFade = StartCoroutine(RunFade(fadeDuration, 1.0f, completeCallback));
        }

        // Fade the screen in from fadeColor.
        // The completeCallback (if not null) is called when the fade is complete, while
        // cancelCallback (if not null) is called when the current fade is canceled (e.g. by
        // starting a new fade).
        public void FadeIn (float fadeDuration, Action completeCallback, Action cancelCallback)
        {
            if (m_activeFade != null)
            {
                CancelActiveFade();
            }

            m_blendMaterial.SetColor("_FadeColor", fadeColor);
            enabled = true;
            m_cancelCallback = cancelCallback;
            m_activeFade = StartCoroutine(RunFade(fadeDuration, 0.0f, completeCallback));
        }

        public void OnRenderImage (RenderTexture src, RenderTexture dest)
        {
            m_blendMaterial.SetFloat("_BlendFactor", m_blendFactor);
            Graphics.Blit(src, dest, m_blendMaterial);
        }

        // ------------------------------------------------------------------

        private void CancelActiveFade ()
        {
            StopCoroutine(m_activeFade);
            m_activeFade = null;

            if (m_cancelCallback != null)
            {
                m_cancelCallback();
                m_cancelCallback = null;
            }
        }

        private IEnumerator RunFade (float fadeDuration, float targetBlendFactor, Action completeCallback)
        {
            float minFactor = 0.0f;
            float maxFactor = 1.0f;
            targetBlendFactor = Mathf.Clamp(targetBlendFactor, minFactor, maxFactor);
            float baseStep = (targetBlendFactor - m_blendFactor) / fadeDuration;

            if (targetBlendFactor > m_blendFactor)
            {
                maxFactor = targetBlendFactor;
            }
            else
            {
                minFactor = targetBlendFactor;
            }

            while (fadeDuration >= 0.0f)
            {
                float step = baseStep * Time.deltaTime;
                m_blendFactor = Mathf.Clamp(m_blendFactor + step, minFactor, maxFactor);
                fadeDuration -= Time.deltaTime;

                yield return null;
            }

            // render final image with targetBlendFactor
            m_blendFactor = targetBlendFactor;
            yield return null;

            enabled = false;
            m_activeFade = null;
            m_cancelCallback = null;

            if (completeCallback != null)
                completeCallback();
        }
    };

} // namespace EAC.Rendering
