
namespace EAC.Rendering
{
    // unity namespaces
    using UnityEngine.Assertions;
    using UnityEngine.EventSystems;
    using UnityEngine;
    // local namespaces
    using EAC.Utility;

    // ======================================================================
    // PointerHoverHighlight
    //
    // Enables/disables a highlight when the pointer enter/exits the object.

    public class PointerHoverHighlight
        : MonoBehaviour
        , IPointerEnterHandler
        , IPointerExitHandler
    {
        [Tooltip("GameObject that will be highlighted (default: this)")]
        public GameObject target;
        [Tooltip("Material used for highlighting (only used if target does not have "
            + "a DrawHighlightOverlay component with highlightMaterial set).")]
        public Material highlightMaterial;

        private DrawHighlightOverlay m_drawHighlight;

        protected void Awake ()
        {
            if (target == null)
                target = gameObject;

            m_drawHighlight = ObjectFinder.GetOrCreateComponent<DrawHighlightOverlay>(target);
            m_drawHighlight.enabled = false;

            if (m_drawHighlight.highlightMaterial == null)
            {
                if (highlightMaterial != null)
                {
                    m_drawHighlight.highlightMaterial = highlightMaterial;
                }
                else
                {
                    EACDebug.LogError(
                        "No highlightMaterial set on PointerHoverHighlight nor DrawHighlightOverlay!");
                }
            }
        }

        protected void OnDisable ()
        {
            m_drawHighlight.enabled = false;
        }

        public void OnPointerEnter (PointerEventData eventData)
        {
            m_drawHighlight.enabled = true;
        }

        public void OnPointerExit (PointerEventData eventData)
        {
            m_drawHighlight.enabled = false;
        }
    };

} // namespace EAC.Rendering
