
namespace EAC.Rendering
{
    // unity namespaces
    using UnityEngine.EventSystems;
    using UnityEngine;

    // ======================================================================
    // PointerHoverAnimate
    //
    // Start/Stop an animation when the pointer enters/exits the object.
    // Requires an Animator with a 'Bool' parameter named 'Playing'.

    [RequireComponent(typeof(Animator))]
    public class PointerHoverAnimate
        : MonoBehaviour
        , IPointerEnterHandler
        , IPointerExitHandler
    {
        private Animator m_animator;
        private static int m_paramIdPlaying = Animator.StringToHash("Playing");

        public void Start ()
        {
            m_animator = GetComponent<Animator>();
            m_animator.SetBool(m_paramIdPlaying, false);
        }

        public void OnPointerEnter (PointerEventData eventData)
        {
            m_animator.SetBool(m_paramIdPlaying, true);
        }

        public void OnPointerExit (PointerEventData eventData)
        {
            m_animator.SetBool(m_paramIdPlaying, false);
        }
    };

} // namespace EAC.Rendering
