
namespace EAC.Rendering
{
    // unity namespaces
    using UnityEngine.Assertions;
    using UnityEngine;
    // local namespaces
    using EAC.Utility;

    // ======================================================================

    public class DrawSkinnedHighlightOverlay
        : MonoBehaviour
    {
        public Material highlightMaterial;

        private MaterialPropertyBlock m_properties;
        private SkinnedMeshRenderer   m_skinnedMeshRenderer;

        private static int m_propIdAmount = -1;
        private static int m_propIdGlowBase = -1;
        private static AnimationCurve m_curveAmount = null;
        private static AnimationCurve m_curveGlowBase = null;

        protected void Awake ()
        {
            // initialize static members if necessary
            // Shader.PropertyToID is not allowed during static init therefore this needs to happen
            // at "runtime".
            if (m_propIdAmount == -1)
                m_propIdAmount = Shader.PropertyToID("_Amount");

            if (m_propIdGlowBase == -1)
                m_propIdGlowBase = Shader.PropertyToID("_GlowBase");

            if (m_curveAmount == null)
            {
                Keyframe[] frameAmount = new Keyframe[3];
                frameAmount[0].time = 0.0f;
                frameAmount[0].value = 0.05f;
                frameAmount[1].time = 0.5f;
                frameAmount[1].value = 0.075f;
                frameAmount[2].time = 1.0f;
                frameAmount[2].value = 0.05f;
                m_curveAmount = new AnimationCurve(frameAmount);
                m_curveAmount.postWrapMode = WrapMode.Loop;
                m_curveAmount.preWrapMode = WrapMode.Loop;
            }

            if (m_curveGlowBase == null)
            {
                Keyframe[] frameGlowBase = new Keyframe[3];
                frameGlowBase[0].time = 0.0f;
                frameGlowBase[0].value = 0.2f;
                frameGlowBase[1].time = 0.5f;
                frameGlowBase[1].value = 0.5f;
                frameGlowBase[2].time = 1.0f;
                frameGlowBase[2].value = 0.2f;
                m_curveGlowBase = new AnimationCurve(frameGlowBase);
                m_curveGlowBase.postWrapMode = WrapMode.Loop;
                m_curveGlowBase.preWrapMode = WrapMode.Loop;
            }
        }

        protected void Start ()
        {
            m_properties = new MaterialPropertyBlock();
            m_properties.SetFloat(m_propIdAmount, 0.0f);
            m_properties.SetFloat(m_propIdGlowBase, 0.0f);

            // replace all materials by highlightMaterial
            Material[] materials = m_skinnedMeshRenderer.sharedMaterials;
            for (int i = 0; i < materials.Length; ++i)
                materials[i] = highlightMaterial;

            m_skinnedMeshRenderer.materials = materials;
            m_skinnedMeshRenderer.SetPropertyBlock(m_properties);
        }

        protected void Update ()
        {
            float amount   = m_curveAmount.Evaluate(Time.time);
            float glowBase = m_curveGlowBase.Evaluate(Time.time);
            m_properties.SetFloat(m_propIdAmount, amount);
            m_properties.SetFloat(m_propIdGlowBase, glowBase);
            m_skinnedMeshRenderer.SetPropertyBlock(m_properties);
        }

        protected void OnEnable ()
        {
            if (m_skinnedMeshRenderer == null)
            {
                m_skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();
                Assert.IsNotNull(m_skinnedMeshRenderer);
            }

            m_skinnedMeshRenderer.enabled = true;
        }

        protected void OnDisable ()
        {
            m_skinnedMeshRenderer.enabled = false;
        }
    };

} // namespace EAC.Rendering
