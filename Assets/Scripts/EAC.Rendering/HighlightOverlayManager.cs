
namespace EAC.Rendering
{
    // unity namespaces
    using UnityEngine;
    // local namespaces
    using EAC.Utility;

    // ======================================================================
    // HighlightOverlayManager

    public class HighlightOverlayManager
    {
        public Material highlightMaterial
        {
            get
            {
                return m_highlightMaterial;
            }

            set
            {
                if(value != m_highlightMaterial)
                {
                    RemoveHighlight(m_target, m_highlightMaterial);
                    m_highlightMaterial = value;
                    AddHighlight(m_target, m_highlightMaterial);
                }
            }
        }

        public GameObject target
        {
            get
            {
                return m_target;
            }

            set
            {
                if(value != m_target)
                {
                    RemoveHighlight(m_target, m_highlightMaterial);
                    m_target = value;
                    AddHighlight(m_target, m_highlightMaterial);
                }
            }
        }

        private Material m_highlightMaterial = null;
        private GameObject m_target = null;

        // ------------------------------------------------------------------

        private void AddHighlight (GameObject targetGO, Material material)
        {
            if(targetGO == null || material == null)
                return;

            AddHighlightOverlayVisitor visitor = new AddHighlightOverlayVisitor(material);
            visitor.Traverse(targetGO);
        }

        private void RemoveHighlight (GameObject targetGO, Material material)
        {
            if(targetGO == null || material == null)
                return;

            RemoveHighlightOverlayVisitor visitor = new RemoveHighlightOverlayVisitor();
            visitor.Traverse(targetGO);
        }
    };

    // ======================================================================

    internal class AddHighlightOverlayVisitor
        : GameObjectVisitor
    {
        private Material m_material;

        public AddHighlightOverlayVisitor (Material material)
        {
            m_material = material;
        }

        protected override bool Visit (GameObject go, int depth)
        {
            MeshFilter filter = go.GetComponent<MeshFilter>();

            if(filter != null)
            {
                DrawHighlightOverlay dho = go.GetComponent<DrawHighlightOverlay>();

                if(dho == null)
                    dho = go.AddComponent<DrawHighlightOverlay>();

                dho.highlightMaterial = m_material;
                dho.enabled = true;
            }

            return true;
        }
    };

    // ===========================================================================================

    internal class RemoveHighlightOverlayVisitor
        : GameObjectVisitor
    {
        protected override bool Visit (GameObject go, int depth)
        {
            MeshFilter filter = go.GetComponent<MeshFilter>();

            if (filter != null)
            {
                DrawHighlightOverlay dho = go.GetComponent<DrawHighlightOverlay>();

                if (dho != null)
                {
                    dho.enabled = false;
                }
            }

            return true;
        }
    };

} // namespace EAC.Rendering
