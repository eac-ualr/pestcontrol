
namespace EAC.Rendering
{
    // system namespaces
    using System.Collections.Generic;
    // unity namespaces
    using UnityEngine.EventSystems;
    using UnityEngine;
    // local namespaces
    using EAC.Utility;

    // ======================================================================
    // PointerHoverHighlightHierarchy

    public class PointerHoverHighlightHierarchy
        : MonoBehaviour
        , IPointerEnterHandler
        , IPointerExitHandler
    {
        [Tooltip("GameObject that will be highlighted (default: this)")]
        public GameObject target;
        [Tooltip("Material used for highlighting")]
        public Material hightlightMaterial;

        private HashSet<GameObject> m_highlightObjects = new HashSet<GameObject>();

        #region Unity Messages
        protected void Awake ()
        {
            if (target == null)
                target = gameObject;
        }

        protected void OnEnable ()
        {
            m_highlightObjects.Clear();
            AddHighlightOverlayVisitor visitor
                = new AddHighlightOverlayVisitor(hightlightMaterial, m_highlightObjects);
            visitor.Traverse(target);
        }

        protected void OnDisable ()
        {
            SubHighlightOverlayVisitor visitor = new SubHighlightOverlayVisitor(m_highlightObjects);
            visitor.Traverse(target);

            m_highlightObjects.Clear();
        }
        #endregion

        public void OnPointerEnter (PointerEventData eventData)
        {
            UpdateHighlightOverlayVisitor visitor = new UpdateHighlightOverlayVisitor(true);
            visitor.Traverse(target);
        }

        public void OnPointerExit (PointerEventData eventData)
        {
            UpdateHighlightOverlayVisitor visitor = new UpdateHighlightOverlayVisitor(false);
            visitor.Traverse(target);
        }

        // ------------------------------------------------------------------

        private class AddHighlightOverlayVisitor
            : GameObjectVisitor
        {
            private Material m_highlightMaterial;
            private List<GameObject> m_skinnedMeshObjects = new List<GameObject>();
            private HashSet<GameObject> m_highlightObjects;

            public AddHighlightOverlayVisitor(Material highlightMaterial, HashSet<GameObject> hilightObjects)
            {
                m_highlightMaterial = highlightMaterial;
                m_highlightObjects = hilightObjects;
            }

            protected override bool Visit (GameObject go, int depth)
            {
                if (go.GetComponent<MeshFilter>() != null)
                {
                    DrawHighlightOverlay dho = go.GetComponent<DrawHighlightOverlay>();

                    if (dho == null)
                        dho = go.AddComponent<DrawHighlightOverlay>();

                    dho.highlightMaterial = m_highlightMaterial;
                    dho.enabled = false;
                }
                else if (go.GetComponent<SkinnedMeshRenderer>() != null)
                {
                    m_skinnedMeshObjects.Add(go);
                }

                return true;
            }

            protected override void PostTraverse ()
            {
                foreach (GameObject go in m_skinnedMeshObjects)
                {
                    GameObject highlightGO = new GameObject(go.name + " Hilight");
                    highlightGO.transform.SetParent(go.transform.parent, false);

                    SkinnedMeshRenderer smr = go.GetComponent<SkinnedMeshRenderer>();
                    SkinnedMeshRenderer highlightSMR = highlightGO.AddComponent<SkinnedMeshRenderer>();
                    highlightSMR.sharedMesh = smr.sharedMesh;
                    highlightSMR.bones = smr.bones;
                    highlightSMR.sharedMaterials = smr.sharedMaterials;

                    DrawSkinnedHighlightOverlay dsho
                        = highlightGO.AddComponent<DrawSkinnedHighlightOverlay>();
                    dsho.highlightMaterial = m_highlightMaterial;
                    dsho.enabled = false;

                    // store created object
                    m_highlightObjects.Add(highlightGO);
                }
            }
        };

        // ------------------------------------------------------------------

        class SubHighlightOverlayVisitor
            : GameObjectVisitor
        {
            private HashSet<GameObject> m_highlightObjects;

            public SubHighlightOverlayVisitor(HashSet<GameObject> hilightObjects)
            {
                m_highlightObjects = hilightObjects;
            }

            protected override bool Visit (GameObject go, int depth)
            {
                DrawHighlightOverlay dho = go.GetComponent<DrawHighlightOverlay>();

                if (dho != null)
                    GameObject.Destroy(dho);

                if (m_highlightObjects.Contains(go))
                    GameObject.Destroy(go);

                return true;
            }
        };

        // ------------------------------------------------------------------

        class UpdateHighlightOverlayVisitor
            : GameObjectVisitor
        {
            private bool m_enable;

            public UpdateHighlightOverlayVisitor(bool enable)
            {
                m_enable = enable;
            }

            protected override bool Visit (GameObject go, int depth)
            {
                DrawHighlightOverlay dho = go.GetComponent<DrawHighlightOverlay>();

                if (dho != null)
                    dho.enabled = m_enable;

                DrawSkinnedHighlightOverlay dsho = go.GetComponent<DrawSkinnedHighlightOverlay>();

                if (dsho != null)
                    dsho.enabled = m_enable;

                return true;
            }
        };
    };

} // namespace EAC.Rendering
