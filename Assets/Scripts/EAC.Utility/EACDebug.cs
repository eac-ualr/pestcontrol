﻿
namespace EAC.Utility {

// unity namespaces
using UnityEngine;

// ===========================================================================
// EACDebug

public static class EACDebug
{
    public static void Log (string message) {
        Debug.Log(string.Format("{0} #{1}: {2}", Time.time, Time.frameCount, message));
    }

    public static void LogFormat (string format, params object[] args) {
        Debug.Log(
            string.Format("{0} #{1}:", Time.time, Time.frameCount)
            + string.Format(format, args));
    }

    public static void LogWarning (string message) {
        Debug.LogWarning(string.Format("{0} #{1}: {2}", Time.time, Time.frameCount, message));
    }

    public static void LogWarningFormat (string format, params object[] args) {
        Debug.LogWarning(
            string.Format("{0} #{1}:", Time.time, Time.frameCount)
            + string.Format(format, args));
    }

    public static void LogError (string message) {
        Debug.LogError(string.Format("{0} #{1}: {2}", Time.time, Time.frameCount, message));
    }

    public static void LogErrorFormat (string format, params object[] args) {
        Debug.LogError(
            string.Format("{0} #{1}:", Time.time, Time.frameCount)
            + string.Format(format, args));
    }
};

} // namespace EAC.Utility
