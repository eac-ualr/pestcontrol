﻿Shader "EAC/EACHighlightShader"
{
    Properties
    {
        _Amount    ("Extrusion Amount", Float)  = 0.01
        _GlowColor ("Glow Color", Color)        = (0.0, 0.0, 0.0, 0.0)
        _GlowBase  ("Glow Base",  Float)        = 0.0
        _GlowPower ("Glow Power", Float)        = 3.0
    }

    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue" = "Geometry+1"}
        LOD 200
        Blend   One One, One Zero
        BlendOp Add
        Cull Back
        ZWrite On

        CGPROGRAM
        #pragma surface surf Lambert alpha vertex:vert

        struct Input {
            float3 viewDir;
        };

        float  _Amount;
        fixed4 _GlowColor;
        fixed  _GlowBase;
        fixed  _GlowPower;

        void vert(inout appdata_full v)
        {
            v.vertex.xyz += _Amount * v.normal;
        }

        void surf(Input IN, inout SurfaceOutput o)
        {
            fixed glow  = 1.0 - saturate(abs(dot(normalize(IN.viewDir), o.Normal)));
            fixed pglow = saturate(_GlowBase + pow(glow, _GlowPower));

            o.Emission = _GlowColor.rgb * pglow;
            o.Alpha    = pglow;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
